# awesome-copyleft-functional-programming
Haskell, purescript, rust, elm, ocaml, scala projects using copyleft

ocaml

* glondu/belenios - voting
* kevinschoon/note - notes
* khady/modern-ocaml - starter ; /ocaml-raytracing; /diffwhat
* hatsugai/syncstitch - refinement checker
* mbodin/tujkuko - recipes ; /blocklib - interface builder; /murder-generator random gen
* gopiandcode/fun-ip - ipv4
* gopiandcode/gopcaml-mode - structural OCaml editing plugin for Emacs 
* ASSETLID/Aych.builder - 
* cfcs/ocaml-socks ; /ocaml-xi-rope - editor
* ArulselvanMadhavan/ocaml-pfds - data structures
* tsileo/entries.pub - indieweb blog
* p2pcollab
* mlin/PhyloCSF 
* examachine/parallpairs

haskell 

* git-annex
* tasty-discover
* ... Firefox rust bits
* Bitwarden-rs
* Webauthn-rs
* wireapp/wire-server
* colah/ImplicitCAD
* luna/luna-studio
* lspitzner/brittany
* crytic/echidna
* jonschoning/espial
* david-christiansen/pie-hs
* pandoc
* naproche-sad
* configifier
* diff-parse
* email-validator
* espial
* fca
* feed-translator
* git-lfs
* git-repair
* gloss-sodium
* haeredes
* hahp
* hannahci
* hath
* hevm
* hopenpgp-tools
* informative
* ipfs
* keysafe
* khph
* lang
* lp-diagrams
* mailbox-count
* nicovideo-translator
* nix-free-tree
* persona
* pg-harness
* purebred-email
* quenya-verb
* semdoc
* spline3
* tasty-tmux
* tax
* tempus-fugit
* unbreak
* wai-static-cache
* yaya
* yggdrasil
* aula
